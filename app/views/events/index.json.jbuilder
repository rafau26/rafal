json.array!(@events) do |event|
  json.extract! event, :id, :title, :description, :picture, :start_date, :start_time, :end_time, :likes, :user_id, :tag_id, :location_id, :comment_id
  json.url event_url(event, format: :json)
end
