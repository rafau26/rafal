class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :title
      t.text :description
      t.string :picture
      t.date :start_date
      t.time :start_time
      t.time :end_time
      t.integer :likes
      t.integer :user_id
      t.integer :tag_id
      t.integer :location_id
      t.integer :comment_id

      t.timestamps
    end
  end
end
