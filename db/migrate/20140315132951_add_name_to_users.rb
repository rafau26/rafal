class AddNameToUsers < ActiveRecord::Migration
  def change
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :picture, :string
    add_column :users, :birthday, :date
    add_column :users, :city, :string
    add_column :users, :postcode, :integer
    add_column :users, :phone, :integer
    add_column :users, :status, :boolean


  end
end
